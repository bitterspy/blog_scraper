from django.shortcuts import render
from django.urls import path, include
from django.contrib import admin
from stats.views import IndexView, run_scraper, words, authors, words_per_author

from django.conf.urls import url
from rest_framework import routers

app_name = 'stats'

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', IndexView.as_view(), name='home'),
    path('run_scrap', run_scraper, name='run_scraper'),
    path('stats/', words, name='words'),
    path('authors/', authors, name='authors'),
    path('stats/<author>', words_per_author, name='words_per_author'),
    # url(r'^api-auth/', include('rest_framework.urls', namespace='rest_framework'))
]