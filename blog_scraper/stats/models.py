from django.utils.html import format_html
from django.db import models
from scrapy_djangoitem import DjangoItem
from scrapy import Item

class Author(models.Model):
    name = models.CharField(max_length=50)
    short_name = models.CharField(max_length=50, default='')


    def __str__(self):
        return self.name


class Blog(models.Model):
    title = models.CharField(max_length=200)
    author = models.ForeignKey(Author, on_delete=models.CASCADE, default='')
    text = models.TextField()

    def __str__(self):
        return self.title
