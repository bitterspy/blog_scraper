from scrapy.crawler import CrawlerProcess
from scrapy.utils.project import get_project_settings
from spiders.blog_spider import BlogSpider

process = CrawlerProcess(get_project_settings())
process.crawl(BlogSpider)
process.start()