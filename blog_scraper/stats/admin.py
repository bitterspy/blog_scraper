from django.contrib import admin
from .models import Blog, Author

class BlogAdmin(admin.ModelAdmin):
    search_fields = ['title']
    ordering = ['title']

class AuthorAdmin(admin.ModelAdmin):
    search_fields = ['name','short_name']
    ordering = ['name','short_name']

admin.site.register(Blog, BlogAdmin)
admin.site.register(Author, AuthorAdmin)