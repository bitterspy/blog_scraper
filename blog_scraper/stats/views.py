from django.shortcuts import render
from django.views.generic import TemplateView
# from django.http import HttpResponse

from twisted.internet import reactor
from scrapy.crawler import CrawlerRunner
from scrapy.utils.project import get_project_settings

from rest_framework.decorators import api_view
from rest_framework.response import Response

from collections import Counter
import json

from .models import Blog, Author
from django.http import HttpResponse

class IndexView(TemplateView):
    info = 'Koza'
    template_name = 'index.html'

def run_scraper(request):

    return render(request, 'index.html', {'info': 'Data from best blog in the World has been scraped!'})

#
#     runner = CrawlerRunner(get_project_settings())
#
#     @defer.inlineCallbacks
#     def crawl():
#         for data in to_crawl:
#             yield runner.crawl('blog_spider', **data)
#         reactor.stop()
#     crawl()
#     reactor.run()

@api_view()
def words(request):
    all_articles=''
    content = Blog.objects.all().values_list('text', flat=True)

    # merge all records in one string
    count = 0
    for articles in content:
        all_articles += articles
        all_articles = all_articles + " "
        count += 1

    words_split = all_articles.split()
    counter = Counter(words_split)
    most_frequent_word = counter.most_common(10)
    return Response(dict(most_frequent_word))

@api_view()
def words_per_author(request, author):
    all_articles=''
    content = Blog.objects.filter(author__short_name=author).values_list('text', flat=True)
    # content = Blog.objects.all().values_list('text', flat=True)

    # merge all records in one string
    count = 0
    for articles in content:
        all_articles += articles
        all_articles = all_articles + " "
        count += 1

    words_split = all_articles.split()
    counter = Counter(words_split)
    most_frequent_word = counter.most_common(10)
    # return HttpResponse(content[0])
    return Response(dict(most_frequent_word))

@api_view()
def authors(request):
    content = Author.objects.all().values_list('short_name','name')
    return Response(dict(content))