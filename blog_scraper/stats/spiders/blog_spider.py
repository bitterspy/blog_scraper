import scrapy
import psycopg2
import re

class BlogSpider(scrapy.Spider):
    name = "blog"
    start_urls = ['http://teonite.com/blog/page/01/']

    conn = psycopg2.connect("host=localhost dbname=blog user=admin password=admin")
    cursor = conn.cursor()

    # parse main pages
    def parse(self, response):
        # parse main blog page to find all articles links
        for article in response.css('article'):
            #find link to article to parsing
            article_link = response.urljoin(article.css("a.read-more::attr(href)")[0].extract())
            # call parse_internal_link function to parse scrap body
            yield scrapy.Request(url=article_link, callback=self.parse_internal_link)


        # find if there is next page of main page
        next_page = response.css('nav.pagination a.older-posts::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    # parse article page
    def parse_internal_link(self,response):
        title = response.css("h1.post-title::text")[0].extract()

        # check if article with given title exist in database to avoid duplicates
        query = ("SELECT title FROM stats_blog WHERE title=%s")
        data = (title,)
        self.cursor.execute(query, data)
        row = self.cursor.fetchone()

        # scrap amd save data into database if there is no article with this title
        if row is None:
            author_name = response.css('span.author-content h4::text')[0].extract()
            # extract content and remove '/n'
            text_list = [element.replace('\n','').strip() for element in response.css('section.post-content ::text').extract()]
            # make string from list elements
            text  = ''.join(text_list)

            # remove all 'non-letters' chars
            text = re.sub('\s+', ' ', text) # condense all whitespaces
            text = re.sub('[^A-Za-z ]+', '', text) #remove non-alpha chars


            # if author doesn't exist
            query = ("SELECT * FROM stats_author WHERE name=%s")
            data = (author_name,)
            self.cursor.execute(query, data)
            row = self.cursor.fetchone()
            if row is None:
                query = "INSERT INTO stats_author (name, short_name) VALUES (%s, %s) RETURNING id;"
                short_name = ''.join(author_name.split()).lower()
                data = (author_name, short_name)
                self.cursor.execute(query, data)
                self.conn.commit()
                author = self.cursor.fetchone()[0]
            else:
            # if author exist insert new one into database and get his id
                query = ("SELECT id FROM stats_author WHERE name=%s")
                data = (author_name,)
                self.cursor.execute(query, data)
                row = self.cursor.fetchone()
                author = row[0]


            query = "INSERT INTO stats_blog (title,author_id,text) VALUES (%s, %s, %s);"
            data = (title, author, text)
            self.cursor.execute(query, data)
            self.conn.commit()
            return {'title': title, 'author': author, 'text': text}
