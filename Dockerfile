FROM python:3.6
MAINTAINER wojciech.spychala@wp.pl

WORKDIR /usr/scr/app
COPY requirements.txt ./
RUN pip install -r requirements.txt
COPY . .

EXPOSE 8080
CMD python ./stats/run_crawler.py
