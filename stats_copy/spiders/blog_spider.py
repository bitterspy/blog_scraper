import scrapy
import psycopg2

class BlogSpider(scrapy.Spider):
    name = "blog"
    start_urls = ['http://teonite.com/blog/page/01/']

    conn = psycopg2.connect("host=localhost dbname=blog user=admin password=admin")
    cursor = conn.cursor()
    temp=0

    def parse(self, response):
        # parse main blog page to find all articles links
        for article in response.css('article'):
            # this part is for testing purpose only
            # title = article.css('h2.post-title a::text')[0].extract()
            # link = article.css('a.read-more::attr(href)')[0].extract()
            # yield {'title': title, 'link': link}

            #find link to article to parsing
            article_link = response.urljoin(article.css("a.read-more::attr(href)")[0].extract())
            # call parse_internal_link function to parse scrap body
            yield scrapy.Request(url=article_link, callback=self.parse_internal_link)


        # find if there is next page of main page
        next_page = response.css('nav.pagination a.older-posts::attr(href)').extract_first()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)

    # parse article page
    def parse_internal_link(self,response):
        title = response.css("h1.post-title::text")[0].extract()
        author = response.css('span.author-content h4::text')[0].extract()
        text_list = [element.replace('\n','').strip() for element in response.css('section.post-content ::text').extract()]
        text  = ''.join(text_list)

        query = ("SELECT title FROM stats_blog WHERE title=%s")
        data = (title, )
        self.cursor.execute(query, data)
        row = self.cursor.fetchone()

        # save data into database if there is no blog with this title
        if row is None:
            query = "INSERT INTO stats_blog (title,author,text) VALUES (%s, %s, %s);"
            data = (title, author, text)
            self.cursor.execute(query, data)
            self.conn.commit()
            return {'title': title, 'author': author, 'text': text}
            # yield {'title': title, 'author': author, 'text': text}

    # TEST PURPOSE (VERSION WITHOUT ITEM
    # def parse_internal_link(self,response):
    #     title = response.css("h1.post-title::text")[0].extract()
    #     author = response.css('span.author-content h4::text')[0].extract()
    #     text = response.css('article ::text').extract()
    #     yield {'title': title, 'author': author, 'text': text }
    #