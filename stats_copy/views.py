from django.shortcuts import render
from django.views.generic import ListView, DetailView, TemplateView
from django.http import HttpResponse

from twisted.internet import  reactor
from scrapy.crawler import CrawlerRunner
from scrapy import log, signals
# from scrap.spiders import blog_spider
from scrapy.utils.project import get_project_settings


# from .models import Blog


class IndexView(TemplateView):
    info = 'Koza'
    template_name = 'index.html'


def run_scraper(request):
    runner = CrawlerRunner(get_project_settings())

    @defer.inlineCallbacks
    def crawl():
        for data in to_crawl:
            yield runner.crawl('blog_spider', **data)
        reactor.stop()
    crawl()
    reactor.run()
    return render(request, 'index.html', {'info': 'Data from best blog in the World has been scraped!'})


class RunScrapView(TemplateView):
    pass


# class AuthorListView(ListView):
#     model = Blog
#
# class AuthorDetailView(DetailView):
#     model = Blog

