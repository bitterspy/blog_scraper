import requests
from bs4 import BeautifulSoup
import re

class Scrap:

    # check how many pages is on the blog
    def __init__(self, html):
        self.html = html
        print(self.html)

        # download html code
        page = requests.get(self.html)
        soup = BeautifulSoup(page.text, "html.parser")

        #parse code to find "<span class='page-number'"
        total_pages_with_html = soup.find_all('span', attrs={'class' : 'page-number'})

        # remove tags, split string for separate words and numbers
        total_pages = total_pages_with_html[0].text.split()

        # get last element from list which is number of pages
        self.total_pages = int(total_pages[-1])

    # find all html links to blog articles
    def find_links_on_page(self, page_number):
        print('Links from page: ', page_number)

        # fist page has no page number so there is a different link to it
        if page_number > 1:
            request = str(self.html) + 'page/' + str(page_number) + '/'
            print(request)

            # get html code from page
            html = requests.get(request)
        else:
            # get html code from page
            request = self.html
            print(request)
            html = requests.get(request)

        # parse html
        soup = BeautifulSoup(html.text, "html.parser")

        # initialize list
        self.link = []

        # parse code to  find "<a class='read-more'", under this is html link to article
        # find all links from page
        for read_more in soup.find_all('a', {'class': 'read-more'}):
            #make link
            link2save = 'http://teonite.com' + read_more['href']
            # write links to list
            self.link.append(link2save)

    # parse text from source link
    def parse_text(self, link):
        html = requests.get(link).content
        soup = BeautifulSoup(html, "html.parser")
        self.raw_text = soup.text.strip().replace('\n', '')

    # check author
    def check_author_title(self, html):
         # download html code
        page = requests.get(html)
        soup = BeautifulSoup(page.text, "html.parser")

        #parse code to find "<span class='author-content'" and extract author
        self.author = soup.find('span', attrs={'class': 'author-content'}).find('h4').text.strip()

        #parse code to find "<span class='author-content'" and extract author
        self.title = soup.find('h1', attrs={'class': 'post-title'}).text.strip()

    # grab data from blog to:
    # self.author
    # self.title
    # self.raw_text
    def grab_data(self, total_pages):
        for page in range(total_pages):
            # find all links on page
            self.find_links_on_page(page + 1)
            # grab every link from page
            counter = 0
            for link in self.link:
                if counter > 0: continue
                counter += 1
                self.check_author_title(link)
                # parse text from article
                self.parse_text(link)
#               print(self.author) #temp

# # set blog address
# blog = Scrap('https://teonite.com/blog/')
# print('Total pages: ', blog.total_pages)
#
# #set max number of links per page
# max_links = 20;
#
# # read all pages one by one to copy links
# blog.grab_data(blog.total_pages)
