from django.utils.html import format_html
from django.db import models
from scrapy_djangoitem import DjangoItem
from scrapy import Item

class Blog(models.Model):
    title = models.CharField(default='Tytul', max_length=200, unique=True)
    author = models.CharField(max_length=50)
    text = models.TextField()

    # shows filed name instead of class name
    def __str__(self):
        return "{title}".format(title=self.title)

class ScrapItem(Item):
    django_model = Blog
