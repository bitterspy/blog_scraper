from django.contrib import admin
from .models import Blog


class BlogAdmin(admin.ModelAdmin):
    search_fields = ['author']
    # ordering = ['title']

admin.site.register(Blog, BlogAdmin)